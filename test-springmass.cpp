/** file: test-srpingmass.cpp
 ** brief: Tests the spring mass simulation
 ** author: Andrea Vedaldi
 **/

#include "springmass.h"
#include <stdlib.h>

int main(int argc, char** argv)
{

  SpringMass springmass;

  const double mass = 0.05 ;
  const double radius = 0.02 ;
  const double naturalLength = 0.95 ;
  const double dt = 1.0/30 ;
  const double stiffness = 1;

  
    Mass mass1(Vector2(-0.5,0), Vector2(), mass, radius) ;
    Mass mass2(Vector2(+0.5,0), Vector2(), mass, radius) ;
  
    springmass.mkMass(mass1);
    springmass.mkMass(mass2);
    springmass.mkSpring(0 , 1 , naturalLength , 1.0);
  

  for (int i = 0 ; i < 100 ; ++i) {
    springmass.step(dt) ;
    springmass.display() ;
  }

  return 0 ;
}
