
PROJETO DE PROGRAMAÇÃO ORIENTADA A OBJETOS
==========================================

BOUNCING BALL(PARTE 1)
===============

OBJETIVO DO PROJETO: 
----------

Mostrar a realização de um projeto que abrange o conteúdo de classes e objetos , na linguagem c++ , o projeto detalha graficamente a movimentação de uma bola definida em um determinado espaço .<br>

VERSÃO COMPILADOR :
-------------------

g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
Copyright (C) 2015 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.<br>

Para a execução deste programa usamos a linha de compilação dos programas definada por ‘g++ ball.cpp ball.h test-ball.cpp -o exemplo’.<br>

ARQUIVOS:
---------

arquivo test-ball.cpp :
-----------------------

Arquivo principal onde ocorre a chamada dos métodos , e é instanciado o objeto ;<br> 

ball.h :
--------
 
Onde as classes são definidas , onde contém seus atributos e construtores .<br>

ball.cpp :
-----------

Contém a implementação dos métodos que são responsáveis pela execução do programa , método display() gera a saída de texto das coordenadas da bola ; método step() responsável direto pela movimentação da bola , pois nele é implementado toda lógica física que permite tal ação ; métodos getX , setX , getY , setY , permitem a mudança das variáveis de inicialização ou a utilização delas para outros fins , isso pois as variáveis de inicialização X,Y são atributos protegidos , e só podem ser usados com a implementação dos métodos get e set .<br>

COORDENADAS X,Y GERADAS PELO MÉTODO DISPLAY:
--------------------------------------------

Coordenadas que mostram a variação da bola no eixo x,y.

posição inicial X: 0.2<br>
posicao inicial Y: 0.5<br>
X: 0.21 Y: 0.491222<br>
X: 0.22 Y: 0.471556<br>
X: 0.23 Y: 0.441<br>
X: 0.24 Y: 0.399556<br>
X: 0.25 Y: 0.347222<br>
X: 0.26 Y: 0.284<br>
X: 0.27 Y: 0.209889<br>
X: 0.28 Y: 0.124889<br>
X: 0.29 Y: 0.029<br>
X: 0.3 Y: -0.0777778<br>
X: 0.31 Y: -0.195444<br>
X: 0.32 Y: -0.324<br>
X: 0.33 Y: -0.463444<br>
X: 0.34 Y: -0.613778<br>
X: 0.35 Y: -0.775<br>
X: 0.36 Y: -0.775<br>
X: 0.37 Y: -0.613778<br>
X: 0.38 Y: -0.463444<br>
X: 0.39 Y: -0.324<br>
X: 0.4 Y: -0.195444<br>
X: 0.41 Y: -0.0777778<br>
X: 0.42 Y: 0.029<br>
X: 0.43 Y: 0.124889<br>
X: 0.44 Y: 0.209889<br>
X: 0.45 Y: 0.284<br>
X: 0.46 Y: 0.347222<br>
X: 0.47 Y: 0.399556<br>
X: 0.48 Y: 0.441<br>
X: 0.49 Y: 0.471556<br>
X: 0.5 Y: 0.491222<br>
X: 0.51 Y: 0.5<br>
X: 0.52 Y: 0.497889<br>
X: 0.53 Y: 0.484889<br>
X: 0.54 Y: 0.461<br>
X: 0.55 Y: 0.426222<br>
X: 0.56 Y: 0.380556<br>
X: 0.57 Y: 0.324<br>
X: 0.58 Y: 0.256556<br>
X: 0.59 Y: 0.178222<br>
X: 0.6 Y: 0.089<br>
X: 0.61 Y: -0.0111111<br>
X: 0.62 Y: -0.122111<br>
X: 0.63 Y: -0.244<br>
X: 0.64 Y: -0.376778<br>
X: 0.65 Y: -0.520444<br>
X: 0.66 Y: -0.675<br>
X: 0.67 Y: -0.840444<br>
X: 0.68 Y: -0.840444<br>
X: 0.69 Y: -0.675<br>
X: 0.7 Y: -0.520444<br>
X: 0.71 Y: -0.376778<br>
X: 0.72 Y: -0.244<br>
X: 0.73 Y: -0.122111<br>
X: 0.74 Y: -0.0111111<br>
X: 0.75 Y: 0.089<br>
X: 0.76 Y: 0.178222<br>
X: 0.77 Y: 0.256556<br>
X: 0.78 Y: 0.324<br>
X: 0.79 Y: 0.380556<br>
X: 0.8 Y: 0.426222<br>
X: 0.81 Y: 0.461<br>
X: 0.82 Y: 0.484889<br>
X: 0.83 Y: 0.497889<br>
X: 0.84 Y: 0.5<br>
X: 0.85 Y: 0.491222<br>
X: 0.86 Y: 0.471556<br>
X: 0.87 Y: 0.441<br>
X: 0.88 Y: 0.399556<br>
X: 0.89 Y: 0.347222<br>
X: 0.89 Y: 0.284<br>
X: 0.88 Y: 0.209889<br>
X: 0.87 Y: 0.124889<br>
X: 0.86 Y: 0.029<br>
X: 0.85 Y: -0.0777778<br>
X: 0.84 Y: -0.195444<br>
X: 0.83 Y: -0.324<br>
X: 0.82 Y: -0.463444<br>
X: 0.81 Y: -0.613778<br>
X: 0.8 Y: -0.775<br>
X: 0.79 Y: -0.775<br>
X: 0.78 Y: -0.613778<br>
X: 0.77 Y: -0.463444<br>
X: 0.76 Y: -0.324<br>
X: 0.75 Y: -0.195444<br>
X: 0.74 Y: -0.0777778<br>
X: 0.73 Y: 0.029<br>
X: 0.72 Y: 0.124889<br>
X: 0.71 Y: 0.209889<br>
X: 0.7 Y: 0.284<br>
X: 0.69 Y: 0.347222<br>
X: 0.68 Y: 0.399556<br>
X: 0.67 Y: 0.441<br>
X: 0.66 Y: 0.471556<br>
X: 0.65 Y: 0.491222<br>
X: 0.64 Y: 0.5<br>
X: 0.63 Y: 0.497889<br>
X: 0.62 Y: 0.484889<br>
X: 0.61 Y: 0.461<br>
X: 0.6 Y: 0.426222<br>
X: 0.59 Y: 0.380556<br>


GRAFICO DA MOVIMENTAÇÃO DA BOLA:
--------------------------------

![](otavio.png)

DIAGRAMA DE CLASSES:
--------------------

![](class-diagram.png)

SPRING-MASS(PARTE 2)
====================

OBJETIVO DO PROJETO:
----------------
Este projeto tem como objetivo a realização de uma simulação gráfica de várias molas e massas se movimentando simultaneamente, no qual a massas estão interligadas as molas  , respeitando a equações físicas , que determinam o movimento das massas e molas.<br>

Para a construção deste código usamos o conceito de classes e objetos , no qual as classes definem os métodos e atributos da massa , mola . o arquivo que contém as classes é o arquivo springmass.h .<br>

LINHA DE COMANDO(COMPILAÇÃO):
------------------------------

Para a execução deste código usa-se a linha de compilação ‘g++ springmass.cpp test-springmass.cpp exemplo’.<br>

ARQUIVOS:
---------

springmass.h -
--------------
Contém a classe Mass , no qual define os métodos e atributos da massa , ou seja , o que se precisa para a construção e representação de uma massa , que é representada por uma esfera . Classe Spring nela é definida os métodos e atributos referentes a representação e construção das molas .<br> Classe SpringMass essa classe tem como objetivo conectar as massa nas molas , ou seja , cada mola terá duas massas conectadas a suas extremidades , é a classe que fará a simulação dinâmica dos movimentos das massas e molas.<br>   Classe Vector2 responsável pela defnição os operadores de quantificação .<br>

springmass.cpp -
----------------

Este arquivo contém a implementação dos métodos definidos nas classes citadas acima .<br>
teste-springmass.cpp -
----------------------

Neste arquivo ocorre as inicializações de alguns atributos que serão usados na definição da massa , como exemplo radius = 0.02 e mass = 0.05 , e também a inicialização de um atributo da mola , exemplo naturalLenght = 0.95 , assim como a posição inicial da massa . Podemos notar também o loop de repetição FOR que é importante para dar dinâmica a execução do programa , pois ele realiza várias chamadas dos métodos step() responsável por toda a dinâmica das massas e molas , e do método display() responsável por mostrar as coordenadas de todas a massas e molas.<br>

COORDENADAS X,Y E ENERGIA TOTAL DE CADA MOVIMENTO DA MOLA:
----------------------------------------------------------

energia total:0.160268<br>
x1: -0.500556  y1: -0.0009<br>
x2: 0.500556  y2: -0.0009<br>

energia total:0.160758<br>
x1: -0.502227  y1: -0.0018<br>
x2: 0.502227  y2: -0.0018<br>

energia total:0.161527<br>
x1: -0.505049  y1: -0.0027<br>
x2: 0.505049  y2: -0.0027<br>

energia total:0.162665<br>
x1: -0.509107  y1: -0.0036<br>
x2: 0.509107  y2: -0.0036<br>

energia total:0.16432<br>
x1: -0.514536  y1: -0.0045<br>
x2: 0.514536  y2: -0.0045<br>

energia total:0.166724<br>
x1: -0.521529  y1: -0.0054<br>
x2: 0.521529  y2: -0.0054<br>

energia total:0.170223<br>
x1: -0.530342  y1: -0.0063<br>
x2: 0.530342  y2: -0.0063<br>

energia total:0.175332<br>
x1: -0.5413  y1: -0.0072<br>
x2: 0.5413  y2: -0.0072<br>

energia total:0.182815<br>
x1: -0.554816  y1: -0.0081<br>
x2: 0.554816  y2: -0.0081<br>

energia total:0.193803<br>
x1: -0.571399  y1: -0.009<br>
x2: 0.571399  y2: -0.009<br>

energia total:0.209964<br>
x1: -0.591676  y1: -0.0099<br>
x2: 0.591676  y2: -0.0099<br>

energia total:0.233767<br>
x1: -0.616418  y1: -0.0108<br>
x2: 0.616418  y2: -0.0108<br>

energia total:0.268854<br>
x1: -0.646566  y1: -0.0117<br>
x2: 0.646566  y2: -0.0117<br>

energia total:0.320609<br>
x1: -0.683267  y1: -0.0126<br>
x2: 0.683267  y2: -0.0126<br>

energia total:0.39698<br>
x1: -0.727919  y1: -0.0135<br>
x2: 0.727919  y2: -0.0135<br>

energia total:0.509708<br>
x1: -0.782224  y1: -0.0144<br>
x2: 0.782224  y2: -0.0144<br>

energia total:0.676137<br>
x1: -0.848253  y1: -0.0153<br>
x2: 0.848253  y2: -0.0153<br>

energia total:0.921879<br>
x1: -0.928523  y1: -0.0162<br>
x2: 0.928523  y2: -0.0162<br>

energia total:1.08872<br>
x1: -0.928523  y1: -0.0171<br>
x2: 0.928523  y2: -0.0171<br>

energia total:0.745739<br>
x1: -0.832252  y1: -0.018<br>
x2: 0.832252  y2: -0.018<br>

energia total:0.529855<br>
x1: -0.755281  y1: -0.0189<br>
x2: 0.755281  y2: -0.0189<br>

energia total:0.394038<br>
x1: -0.693503  y1: -0.0198<br>
x2: 0.693503  y2: -0.0198<br>

energia total:0.308688<br>
x1: -0.643634  y1: -0.0207<br>
x2: 0.643634  y2: -0.0207<br>

energia total:0.255181<br>
x1: -0.603032  y1: -0.0216<br>
x2: 0.603032  y2: -0.0216<br>

energia total:0.221819<br>
x1: -0.569564  y1: -0.0225<br>
x2: 0.569564  y2: -0.0225<br>

energia total:0.201279<br>
x1: -0.54149  y1: -0.0234<br>
x2: 0.54149  y2: -0.0234<br>

energia total:0.189012<br>
x1: -0.517368  y1: -0.0243<br>
x2: 0.517368  y2: -0.0243<br>

energia total:0.182248<br>
x1: -0.495987  y1: -0.0252<br>
x2: 0.495987  y2: -0.0252<br>

energia total:0.179384<br>
x1: -0.476299  y1: -0.0261<br>
x2: 0.476299  y2: -0.0261<br>

energia total:0.179617<br>
x1: -0.457369  y1: -0.027<br>
x2: 0.457369  y2: -0.027<br>

energia total:0.182744<br>
x1: -0.438329  y1: -0.0279<br>
x2: 0.438329  y2: -0.0279<br>

energia total:0.189092<br>
x1: -0.418335  y1: -0.0288<br>
x2: 0.418335  y2: -0.0288<br>

energia total:0.19953<br>
x1: -0.396534  y1: -0.0297<br>
x2: 0.396534  y2: -0.0297<br>

energia total:0.215591<br>
x1: -0.372021  y1: -0.0306<br>
x2: 0.372021  y2: -0.0306<br>

energia total:0.239693<br>
x1: -0.343802  y1: -0.0315<br>
x2: 0.343802  y2: -0.0315<br>

energia total:0.275504<br>
x1: -0.310756  y1: -0.0324<br>
x2: 0.310756  y2: -0.0324<br>

energia total:0.328507<br>
x1: -0.271585  y1: -0.0333<br>
x2: 0.271585  y2: -0.0333<br>

energia total:0.406833<br>
x1: -0.224766  y1: -0.0342<br>
x2: 0.224766  y2: -0.0342<br>

energia total:0.522522<br>
x1: -0.16849  y1: -0.0351<br>
x2: 0.16849  y2: -0.0351<br>

energia total:0.693367<br>
x1: -0.100593  y1: -0.036<br>
x2: 0.100593  y2: -0.036<br>

energia total:0.945661<br>
x1: -0.0184694  y1: -0.0369<br>
x2: 0.0184694  y2: -0.0369<br>

energia total:1.01035<br>
x1: 0.0810245  y1: -0.0378<br>
x2: -0.0810245  y2: -0.0378<br>

energia total:0.703505<br>
x1: 0.180582  y1: -0.0387<br>
x2: -0.180582  y2: -0.0387<br>

energia total:0.513108<br>
x1: 0.263514  y1: -0.0396<br>
x2: -0.263514  y2: -0.0396<br>

energia total:0.397378<br>
x1: 0.334099  y1: -0.0405<br>
x2: -0.334099  y2: -0.0405<br>

energia total:0.33064<br>
x1: 0.395911  y1: -0.0414<br>
x2: -0.395911  y2: -0.0414<br>

energia total:0.297665<br>
x1: 0.452011  y1: -0.0423<br>
x2: -0.452011  y2: -0.0423<br>

energia total:0.290246<br>
x1: 0.505094  y1: -0.0432<br>
x2: -0.505094  y2: -0.0432<br>

energia total:0.305268<br>
x1: 0.557627  y1: -0.0441<br>
x2: -0.557627  y2: -0.0441<br>

energia total:0.34379<br>
x1: 0.611965  y1: -0.045<br>
x2: -0.611965  y2: -0.045<br>

energia total:0.410953<br>
x1: 0.670458  y1: -0.0459<br>
x2: -0.670458  y2: -0.0459<br>

energia total:0.516591<br>
x1: 0.735558  y1: -0.0468<br>
x2: -0.735558  y2: -0.0468<br>

energia total:0.67665<br>
x1: 0.809924  y1: -0.0477<br>
x2: -0.809924  y2: -0.0477<br>

energia total:0.915569<br>
x1: 0.896531  y1: -0.0486<br>
x2: -0.896531  y2: -0.0486<br>

energia total:1.07667<br>
x1: 0.896531  y1: -0.0495<br>
x2: -0.896531  y2: -0.0495<br>

energia total:0.743476<br>
x1: 0.795632  y1: -0.0504<br>
x2: -0.795632  y2: -0.0504<br>

energia total:0.535314<br>
x1: 0.712571  y1: -0.0513<br>
x2: -0.712571  y2: -0.0513<br>

energia total:0.406664<br>
x1: 0.643021  y1: -0.0522<br>
x2: -0.643021  y2: -0.0522<br>

energia total:0.32923<br>
x1: 0.583412  y1: -0.0531<br>
x2: -0.583412  y2: -0.0531<br>

energia total:0.285738<br>
x1: 0.530741  y1: -0.054<br>
x2: -0.530741  y2: -0.054<br>

energia total:0.266113<br>
x1: 0.48242  y1: -0.0549<br>
x2: -0.48242  y2: -0.0549<br>

energia total:0.265198<br>
x1: 0.436147  y1: -0.0558<br>
x2: -0.436147  y2: -0.0558<br>

energia total:0.281502<br>
x1: 0.389793  y1: -0.0567<br>
x2: -0.389793  y2: -0.0567<br>

energia total:0.316677<br>
x1: 0.341299  y1: -0.0576<br>
x2: -0.341299  y2: -0.0576<br>

energia total:0.375593<br>
x1: 0.288588  y1: -0.0585<br>
x2: -0.288588  y2: -0.0585<br>

energia total:0.466972<br>
x1: 0.229466  y1: -0.0594<br>
x2: -0.229466  y2: -0.0594<br>

energia total:0.604658<br>
x1: 0.161533  y1: -0.0603<br>
x2: -0.161533  y2: -0.0603<br>

energia total:0.80971<br>
x1: 0.0820836  y1: -0.0612<br>
x2: -0.0820836  y2: -0.0612<br>

energia total:1.06799<br>
x1: -0.0120038  y1: -0.0621<br>
x2: 0.0120038  y2: -0.0621<br>

energia total:0.735519<br>
x1: -0.103279  y1: -0.063<br>
x2: 0.103279  y2: -0.063<br>

energia total:0.526349<br>
x1: -0.174789  y1: -0.0639<br>
x2: 0.174789  y2: -0.0639<br>

energia total:0.394906<br>
x1: -0.230412  y1: -0.0648<br>
x2: 0.230412  y2: -0.0648<br>

energia total:0.31252<br>
x1: -0.273188  y1: -0.0657<br>
x2: 0.273188  y2: -0.0657<br>

energia total:0.261192<br>
x1: -0.305474  y1: -0.0666<br>
x2: 0.305474  y2: -0.0666<br>

energia total:0.229661<br>
x1: -0.329076  y1: -0.0675<br>
x2: 0.329076  y2: -0.0675<br>

energia total:0.21095<br>
x1: -0.345355  y1: -0.0684<br>
x2: 0.345355  y2: -0.0684<br>

energia total:0.200833<br>
x1: -0.355292  y1: -0.0693<br>
x2: 0.355292  y2: -0.0693<br>

energia total:0.196896<br>
x1: -0.359556  y1: -0.0702<br>
x2: 0.359556  y2: -0.0702<br>

energia total:0.197987<br>
x1: -0.358537  y1: -0.0711<br>
x2: 0.358537  y2: -0.0711<br>

energia total:0.20392<br>
x1: -0.352378  y1: -0.072<br>
x2: 0.352378  y2: -0.072<br>

energia total:0.215379<br>
x1: -0.340989  y1: -0.0729<br>
x2: 0.340989  y2: -0.0729<br>

energia total:0.233975<br>
x1: -0.324048  y1: -0.0738<br>
x2: 0.324048  y2: -0.0738<br>

energia total:0.262467<br>
x1: -0.301001  y1: -0.0747<br>
x2: 0.301001  y2: -0.0747<br>

energia total:0.305169<br>
x1: -0.27104  y1: -0.0756<br>
x2: 0.27104  y2: -0.0756<br>

energia total:0.368604<br>
x1: -0.233079  y1: -0.0765<br>
x2: 0.233079  y2: -0.0765<br>

energia total:0.462498<br>
x1: -0.185716  y1: -0.0774<br>
x2: 0.185716  y2: -0.0774<br>

energia total:0.601281<br>
x1: -0.12718  y1: -0.0783<br>
x2: 0.12718  y2: -0.0783<br>

energia total:0.806297<br>
x1: -0.0552667  y1: -0.0792<br>
x2: 0.0552667  y2: -0.0792<br>

energia total:0.984669<br>
x1: 0.0327447  y1: -0.0801<br>
x2: -0.0327447  y2: -0.0801<br>

energia total:0.684228<br>
x1: 0.119082  y1: -0.081<br>
x2: -0.119082  y2: -0.081<br>

energia total:0.495282<br>
x1: 0.186531  y1: -0.0819<br>
x2: -0.186531  y2: -0.0819<br>

energia total:0.37665<br>
x1: 0.238761  y1: -0.0828<br>
x2: -0.238761  y2: -0.0828<br>

energia total:0.302445<br>
x1: 0.278634  y1: -0.0837<br>
x2: -0.278634  y2: -0.0837<br>

energia total:0.256433<br>
x1: 0.308363  y1: -0.0846<br>
x2: -0.308363  y2: -0.0846<br>

energia total:0.228493<br>
x1: 0.329628  y1: -0.0855<br>
x2: -0.329628  y2: -0.0855<br>

energia total:0.212402<br>
x1: 0.343676  y1: -0.0864<br>
x2: -0.343676  y2: -0.0864<br>

energia total:0.204466<br>
x1: 0.351388  y1: -0.0873<br>
x2: -0.351388  y2: -0.0873<br>

energia total:0.202686<br>
x1: 0.353332  y1: -0.0882<br>
x2: -0.353332  y2: -0.0882<br>

energia total:0.206292<br>
x1: 0.3498  y1: -0.0891<br>
x2: -0.3498  y2: -0.0891<br>

energia total:0.215517<br>
x1: 0.340828  y1: -0.09<br>
x2: -0.340828  y2: -0.09<br>




GRÁFICO QUE REPRESENTA O MOVIMENTO DO MOLA:
-------------------------------------------

![](springmass.png)


DIAGRAMA DE CLASSES:
--------------------

![](class_diagram.png)


GRÁFICOS (PARTE 3)
==================

OBJETIVO:
---------

Essa parte do projeto , tem a finalidade de mostrar ao usuário o funcionamento dos projetos BOUNCING BALL e  SPRINGMASS , porém neste caso , mostrar graficamente qual é o comportamento físico da bola , e  da  mola .<br>

DEPENDÊNCIAS:
-------------

Para a execução dessa parte do projeto , além de ter vinculado , todos os outros arquivos de BOUNCING BALL e  SPRINGMASS já refenciados nas partes 1 e 2 , é necessário a instalação da biblioteca OpenGL/Glut , elas forneceram as condições necessárias para o usuário ver graficamente como se comportam tais objetos , para dependências linux , versão >= 11.10 , as linhas de comando responsáveis pela instalação são :<br>
	
	"sudo apt-get install freeglut3 freeglut3-dev"<br>
	"sudo apt-get install binutils-gold"<br>

	Para versões anteriores a 11.10 ,é necessária apenas a primeira linha de comando citada acima.<br>
				

LINHA DE COMPILAÇÃO:
--------------------

Para a execução do BOUNCING BALL , usamos a linha de compilação:<br>

 "g++ graphics.cpp ball.cpp test-ball-graphics.cpp -o teste -lglut -lGL -lGLU"<br>

Para a execução do SPRINGMASS ,usamps a linha de compilação:<br>

"g++ graphics.cpp springmass.cpp test-springmass-graphics.cpp -o teste -lGL -lGLU -lglut"<br>

ARQUIVOS:
---------
test-ball-graphics.cpp -
--------------------
Arquivo que contém a estrutura principal do programa MAIN, assim como a classe BallDrawable , que é responsável pela passagem dos parâmetros para o método drawCircle , que irá desenha a bola.<br>

test-springmass-graphics.cpp -
--------------------------
Arquivo que contém a estrutura principal do programa MAIN, e a classe SpringMassDrawable , que é responsável pela passagem de parâmetros , para os métodos drawCircle e drawLine , que irão desenhar as massas e molas correspondentes.<br>

graphics.h -
------------
Contém a classe Drawable que defini os métodos responsáveis pelo desenho das massas e molas.

graohics.cpp -
--------------
Este arquivo contém a implementação dos métodos aqui já citados , destacando drawCircle e drawLine , que serão os responsáveis pelo desenho das massas (representadas por bolas) , e das molas (respresentadas por linhas).

SCREEN SHOT:
------------

BOUNCING BALL:
--------------

![](bouncingball.png)<br>


SPRINGMASS :
------------

![](spring_mass.png)<br>
![](spring-mass.png)<br>

DIAGRAMA DE CLASSES :
---------------------

![](class-ball-spring-graphics.png)<br>


