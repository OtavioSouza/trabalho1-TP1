/** file: test-springmass-graphics.cpp
 ** brief: Tests the spring mass simulation with graphics
 ** author: Andrea Vedaldi
 **/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable 
/* ---------------------------------------------------------------- */
{
private:
	Figure figure;

/* INCOMPLETE: TYPE YOUR CODE HERE */
public:
	SpringMassDrawable(): SpringMass(),figure("SpringMass")
	{
    figure.addDrawable(this) ;
  	}

    //método para qual , será responsável por chamar os 
    //métodos que irão construir as duas massas e a mola
  	void draw() {
      //drawCircle() , método responsável pela construção das massas
    	figure.drawCircle(mass1->getPosition().x, mass1->getPosition().y, mass1->getRadius());
    	figure.drawCircle(mass2->getPosition().x, mass2->getPosition().y, mass2->getRadius());
       //drawLine() , método responsável pela construção da mola  
   		figure.drawLine(mass1->getPosition().x, mass1->getPosition().y, mass2->getPosition().x, mass2->getPosition().y , 0.03);
  	}

  	void display() {
    	figure.update() ;
  	}

} ;

int main(int argc, char** argv)
{
	glutInit(&argc,argv) ;
	SpringMassDrawable springmass ;

  const double mass = 0.05 ;
	const double radius = 0.02 ;
  const double naturalLength = 0.95 ;
  const double stiffness = 1;

	Mass mass1(Vector2(-0.5,0), Vector2(), mass, radius);
  Mass mass2(Vector2(0.5,0), Vector2(), mass, radius); 

  springmass.mkMass(mass1);
  springmass.mkMass(mass2);
  springmass.mkSpring(0, 1, naturalLength, 1);
  
  springmass.display();
	springmass.draw();
	
  run(&springmass, 1/60.0) ;//120.0

  return 0;
}
