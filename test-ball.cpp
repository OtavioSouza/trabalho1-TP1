/** file: test-ball.cpp
 ** brief: Tests the bouncing ball simulation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"
#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
  Ball ball ;

  // Alteramos os valores iniciais da bola 
  ball.setX(0.2);
  ball.setY(0.5);

  // valores iniciais mostrados 
  cout <<"posição inicial X"<< endl << ball.getX() << endl;
  cout <<"posicao inicial Y"<< endl <<ball.getY() << endl;

  const double dt = 1.0/30 ;
  //substituimos o loop principal FOR , pelo método run
  //no qual executa a mesma função do loop
  ball.run(dt);
  return 0 ;
}