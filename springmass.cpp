/** file: springmass.cpp
 ** brief: SpringMass simulation implementation
 ** author: Andrea Vedaldi
 **/

#include "springmass.h"

#include <iostream>

using namespace std;

/* ---------------------------------------------------------------- */
// class Mass
/* ---------------------------------------------------------------- */

Mass::Mass()
: position(), velocity(), force(), mass(1), radius(1)
{ }

Mass::Mass(Vector2 position, Vector2 velocity, double mass, double radius)
: position(position), velocity(velocity), force(), mass(mass), radius(radius),
xmin(-1),xmax(1),ymin(-1),ymax(1)
{ }

void Mass::setForce(Vector2 f)
{
  force = f ;
}

void Mass::addForce(Vector2 f)
{
  force = force + f ;
}

Vector2 Mass::getForce() const
{
  return force ;
}

Vector2 Mass::getPosition() const
{
  return position ;
}

Vector2 Mass::getVelocity() const
{
  return velocity ;
}

double Mass::getRadius() const
{
  return radius ;
}

double Mass::getMass() const
{
  return mass ;
}

double Mass::getEnergy(double gravity) const
{
  double energy = 0 ;
  double E_cinetica , E_potencial;
  double altura;

  // calculamos a altura da mola , levando em conta a diferença do raio da massa(esfera) 
  // em relação a posição em que a esfera se encontra(y)
  if(getPosition().y > 0){
    altura = 1 + getPosition().y - radius;
  }
  else{
    altura = 1 - getPosition().y - radius;
  }
  
  //calculo da energia pontencial e cinetica

  // energia pontencial = massa * gravidade * altura 
  E_potencial = mass * gravity * altura;

  //energia cinética = (mass * vel*vel)/2.
  E_cinetica = mass * (velocity.norm2())/2;
  
  energy = E_potencial + E_cinetica;

  return energy;
}

void Mass::step(double dt , double gravity){

  double x, y, velX, velY;
  Spring spring(Mass* mass1,Mass* mass2,double naturalLength,double stiffness,const double damping = 0.01);
  Vector2 aceleracao;

  //CALCULO DA COMPONENTE Y **************************************************************

  //aceleração = force / mass     CALCULO DA ACELERAÇÃO EM Y
  aceleracao.y = getForce().y/getMass();


  //S = S0 + V*dt + (a*dt^2)/2     CALCULO DA POSIÇÃO EM Y
  y = position.y + velY*dt + ((aceleracao.y - gravity )*(dt*dt))/2;

  //V = V0 + a*dt      CALCULO DA VELOCIDADE EM Y
  velY = getVelocity().y +(aceleracao.y * dt);


  if((ymin + getRadius() <= y) && (ymax - getRadius() >= y)){ 
    position.y = y;
    velocity.y = velY;
  }

  else{
    velocity.y = -velY + 1;
  }

  //CALCULO DA COMPONENTE X *****************************************************************

  //aceleração = force / mass      CALCULO DA ACELERAÇÃO EM X
  aceleracao.x = (getForce().x)/getMass();


  //S = S0 + V*dt + (a*dt^2)/2       CALCULO DA POSIÇÃO EM X
  x = position.x + velocity.x*dt + (aceleracao.x * (dt*dt))/2;

  //V = V0 + a*dt      CALCULO DA VELOCIDADE EM X 
  velX = getVelocity().x + (aceleracao.x * dt);


  if((xmin + getRadius() <= x) && (xmax - getRadius() >= x )) { 
    position.x = x;
    velocity.x = velX;
  }
  else{ 
    velocity.x = -velX;
  }

}

/* ---------------------------------------------------------------- */
// class Spring
/* ---------------------------------------------------------------- */

Spring::Spring(Mass * mass1, Mass * mass2, double naturalLength, double stiffness, double damping)
: mass1(mass1), mass2(mass2),
naturalLength(naturalLength), stiffness(stiffness), damping(damping)
{ }

Mass * Spring::getMass1() const
{
  return mass1 ;
}

Mass * Spring::getMass2() const
{
  return mass2 ;
}

Vector2 Spring::getForce() const
{
  Vector2 F ;

  //Este metodo calcula a forca desta mola usando as equacoes para oscilador harmonico amortecido 

  // vetor da distancias das extremidades da mola
  Vector2 vetorU = mass2 -> getPosition() - mass1 -> getPosition() ;

  //vetor unitário da direção da mola
  Vector2 vetorUnitario = vetorU/getLength();

  // calculo do vetor de velocidade entre as massas
  double VelAlongamento = dot((mass2->getVelocity() - mass1 -> getVelocity()), vetorUnitario);
  
  //módulo da força calculado
  F = ((naturalLength - getLength()) * stiffness + VelAlongamento * damping) * vetorUnitario;
  
  return F ;
}

double Spring::getLength() const
{
  Vector2 u = mass2->getPosition() - mass1->getPosition() ;
  return u.norm() ;
}

double Spring::getEnergy() const {
  double length = getLength() ;
  double dl = length - naturalLength;
  return 0.5 * stiffness * dl * dl ;
}

std::ostream& operator << (std::ostream& os, const Mass& m)
{
  os<<"("
  <<m.getPosition().x<<","
  <<m.getPosition().y<<")" ;
  return os ;
}

std::ostream& operator << (std::ostream& os, const Spring& s)
{
  return os<<"$"<<s.getLength() ;
}

/* ---------------------------------------------------------------- */
// class SpringMass : public Simulation
/* ---------------------------------------------------------------- */

SpringMass::SpringMass(double gravity)
:gravity(gravity)
{ }

void SpringMass::display()
{

  //Mostramos aqui as posições das massas e mola , bem como a energia total do sistema 
  
  cout<< "energia total:" << getEnergy() <<"<br>"<<endl;
  cout<<"x1:"<<' '<< mass1->getPosition().x <<' '<<" y1:"<<' '<< mass1->getPosition().y <<"<br>"<<endl;
  cout<<"x2:"<<' '<< mass2->getPosition().x <<' '<<" y2:"<<' '<< mass2->getPosition().y <<"<br>"<<endl;
  cout<<endl;
}

double SpringMass::getEnergy() const
{
  double energy = 0 ;

  //somamos as energias das massa e da mola
  
  energy = mass1->getEnergy(gravity) + mass2->getEnergy(gravity) + spring->getEnergy();
  
  return energy ;
}

void SpringMass::step(double dt)
{
  Vector2 g(0,-gravity) ; // Vetor da gravidade, i.e.,
  // aceleracao apontando para baixo.
  
  double energy = 0;
  int i = 0;

  //aqui a força das massas é atualizada
  while(i < vetMassas.size()){
    mass1 = &(vetMassas[i]);
    mass2 = &(vetMassas[i+1]);
    spring = &(vetSprings[vetSprings.size()-1]);
    mass1->setForce(spring -> getForce());
    mass2->setForce(-1.0*spring->getForce());
    mass1->step(dt,gravity);
    mass2->step(dt,gravity);
    energy = getEnergy();

    i+=2;
  }

}

int SpringMass::mkMass(Mass massas){
   vetMassas.push_back(massas);
   return (int)vetMassas.size()-1;

   //método para inserção de uma massa.
   //Que retorna o numero total de massas.
}

void SpringMass::mkSpring(int i, int j,double naturalLength, double stiffness){
     Spring sp1(&vetMassas[i] ,&vetMassas[j] , naturalLength , stiffness);
     vetSprings.push_back(sp1);

     //método para inserção de uma mola.
     //push_back() método usado para adicionar elementos na estrutura tipo vector
};



