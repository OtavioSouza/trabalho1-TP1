/** file: ball.cpp
 ** brief: Ball class - implementation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"

#include <iostream>

Ball::Ball()
: r(0.1), x(0), y(0), vx(0.3), vy(-0.1), g(9.8), m(1),
xmin(-1), xmax(1), ymin(-1), ymax(1)
{ }

void Ball::step(double dt)
{
  double xp = x + vx * dt ;
  double yp = y + vy * dt - 0.5 * g * dt * dt ;
  if (xmin + r <= xp && xp <= xmax - r) {
    x = xp ;
  } else {
    vx = -vx ;
  }
  if (ymin + r <= yp && yp <= ymax - r) {
    y = yp ;
    vy = vy - g * dt ;
  } else {
    vy = -vy ;
  }
}

void Ball::display()
{
  std::cout<< x <<" "<< y <<std::endl ;
}

void Ball::run(double dt){
  for(int i = 0 ; i < 100 ; i++){
    step(dt);   
    display();
  }
}

// aqui implementamos as funcoes set e get ,
//parar alterar os valores das coordenads X , Y.

double Ball::setX(double x){
  this->x = x;
}

double Ball::setY(double y){
  this -> y = y;
}

double Ball::getX(){
  return x;
}
double Ball::getY(){
  return y;
}

